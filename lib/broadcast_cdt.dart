import 'package:flutter/services.dart';

import 'broadcast_cdt_platform_interface.dart';

class BroadcastCdt {
  Future<String?> getPlatformVersion() {
    return BroadcastCdtPlatform.instance.getPlatformVersion();
  }

  Future<EventChannel?> startLocationReceiver() {
    return BroadcastCdtPlatform.instance.startLocationReceiver();
  }

  Future<bool?> removeLocationReceiver() {
    return BroadcastCdtPlatform.instance.removeLocationReceiver();
  }
}
