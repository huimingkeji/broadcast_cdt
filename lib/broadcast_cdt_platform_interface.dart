import 'package:flutter/services.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'broadcast_cdt_method_channel.dart';

abstract class BroadcastCdtPlatform extends PlatformInterface {
  /// Constructs a BroadcastCdtPlatform.
  BroadcastCdtPlatform() : super(token: _token);

  static final Object _token = Object();

  static BroadcastCdtPlatform _instance = MethodChannelBroadcastCdt();

  /// The default instance of [BroadcastCdtPlatform] to use.
  ///
  /// Defaults to [MethodChannelBroadcastCdt].
  static BroadcastCdtPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [BroadcastCdtPlatform] when
  /// they register themselves.
  static set instance(BroadcastCdtPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String?> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }

  Future<EventChannel?> startLocationReceiver() {
    throw UnimplementedError('startLocationReceiver() has not been implemented.');
  }

  Future<bool?> removeLocationReceiver() {
    throw UnimplementedError('removeLocationReceiver() has not been implemented.');
  }
}
